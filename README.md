## Step 1: Use ntdsutil to get the ntds.dit and SYSTEM hive
On the domain controller, open command prompt and run the following:
```
C:\>ntdsutil

ntdsutil: ifm
ifm: create full c:\audit
ifm: quit
ntdsutil: quit
```
Copy these files to your workstation in a designated working folder.

## Step 2: Download the HIBP hash list
Latest version as of 1/17/2019 [here](https://downloads.pwnedpasswords.com/passwords/pwned-passwords-sha1-ordered-by-count-v4.7z). Afterwards, unzip the file.

## Step 3: Convert ntds.dit file to Hashcat readable format User:Hash
Make sure you have DSInternals installed from [here](https://github.com/MichaelGrafnetter/DSInternals) or if you a running Powershell 5 `Install-Module -Name DSInternals -Force`.
Open powershell as administrator and run the following:
```
$key = Get-BootKey -SystemHivePath .\registry\SYSTEM
Get-ADDBAccount -All -DBPath '.\Active Directory\ntds.dit' -BootKey $key | Format-Custom -View HashcatNT | Out-File hashes.txt -Encoding ASCII
```
## Step 4: Match your hashes against the HIBP hashes
Download altered Match-ADHashes.ps1 from [here](https://gitlab.com/chelmzy/five-minute-password-audit/blob/master/Match-ADHashes.ps1). All credit to [DGG-IT](https://github.com/DGG-IT) for the script. I just altered the output slightly.
Open powershell as administrator and run the following:
```
Import .\Match-ADHashes.ps1
Match-ADHashes -ADNTHashes hashes.txt -HashDictionary pwned-passwords-ntlm-ordered-by-count-v4.txt | Out-File PasswordAudit.csv
```

